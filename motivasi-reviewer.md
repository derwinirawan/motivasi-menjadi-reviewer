Motivasi menjadi penelaah
===

- **Nama**: Dasapta Erwin Irawan
- **Afiliasi**: Fakultas Ilmu dan Teknologi Kebumian
- **Kelompok Keilmuan**: Geologi Terapan
- **Fokus riset**: hidrogeologi, hidrokimia, interaksi air tanah-air sungai, hidrogeologi kawasan gunung api dan hidrogeologi kawasan urban
- **Profil riset**: [ORCID](https://orcid.org/0000-0002-1526-0863), [Google Scholar](https://scholar.google.co.id/citations?user=Myvc78MAAAAJ&hl=en&authuser=2), [Microsoft Academic](https://academic.microsoft.com/profile/8h65g184-j0i8-4f5e-f613-8ej64074f99g/DasaptaErwinIrawan/publication/search?q=Dasapta%20Erwin%20Irawan&qe=%2540%2540%2540USER.PUBLICATIONS%253D8d65c184-f0e8-4b5a-b613-8af64074b99c&f=&orderBy=0), [ScopusID](https://www.scopus.com/authid/detail.uri?authorId=34771445600), dan [Publons/ResearcherID](https://publons.com/researcher/783905/dasapta-erwin-irawan/) 

Reviewer (penelaah) walaupun tidak berfungsi sebagai penjaga pintu gerbang riset atau publikasi, tetapi para peninjau memiliki peran penting sebagai orang kedua yang membaca proposal setelah penulis. Tugasnya adalah untuk:

- menelaah alur riset dan menguji validitas metode, serta mengevaluasi dampaknya kepada obyek riset,
- menilai tingkat originalitas secara relatif kepada populasi riset (berdasarkan analisis bibliometrik) yang dilaksanakan dengan obyek atau lokasi riset yang sama atau berdekatan,
- dan yang terakhir adalah memberikan masukan untuk meningkatkan kualitas proposal. 

Dengan tugas seperti ini, saya terpanggil untuk menjadi penelaah internal Perguruan Tinggi untuk turut serta meningkatkan kualitas riset, terutama untuk kriteria-kriteria yang sering dilupakan (**RDK**):

1. **Reprodusibilitas** (*reproducibility*)
2. **pengelolaan Data riset** (*research data management*)
3. dan teknik **Komunikasi keskolaran** (*scholarly communication*) yang akan dilaksanakan untuk menyampaikan hasil riset kepada masyarakat.

Tentang reprodusibilitas [tautan](https://www.slideshare.net/carolegoble/what-is-reproducibility-gobleclean)

![reproducibility 2](source/images/Reproducibility2.png)

![reproducibility 3](source/images/Reproducibility3.png)

Tentang pengelolaan data riset (Sydney University)

![RDMP Sydney University](https://library.sydney.edu.au/research/data-management/images/Research_data_lifecycle_final_20150827.png)

Tentang komunikasi keskolaran ([tautan](https://figshare.com/articles/101_Innovations_in_Scholarly_Communication_the_Changing_Research_Workflow/1286826))

![scholcomm](source/images/scholcomm.png)

Untuk melaksanakan tugas tersebut, saya akan:

1. memaksimumkan **komunikasi secara terbuka**, bila memungkinkan, dengan penulis proposal,
2. membaca proposal secara seksama dan **membuat peta alur pikirnya** untuk menguji keterkaitan antara pertanyaan riset, metode, rujukan yang digunakan, dan luaran yang diharapkan,
3. melakukan **analisis bibliometrik** sederhana untuk mengetahui:

- **tren riset** di bidang yang sesuai dengan proposal riset,
- **celah riset** yang belum atau belum banyak dilakukan peneliti lainnya,
- **kata kunci** yang berkaitan dengan proposal tetapi belum dinyatakan di dalamnya.

4. menilai komponen RDK yang telah disampaikan di atas,
5. memberikan masukan konstruktif kepada penulis proposal. 
<!--stackedit_data:
eyJoaXN0b3J5IjpbMjk0NzU5NTAxLC0xMjEwODI0MjQwLC0xMT
gyMTczMDA5LC03Njk1MDc4MzMsLTEzMzEwOTI2MzcsNDI2MTI2
NDgzLC05NDg2OTA5MzgsLTE5NzYwOTIzNzMsLTE4NzY3MTc1OT
UsLTEwMTc3ODA0NSwtMTUwNTU4NDcyMSwzNDMwMjczOTRdfQ==

-->